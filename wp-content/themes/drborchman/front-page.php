<?php get_header(); ?>
 	<div id="heroImage">
 		<img src="<?php echo catch_that_image() ?>" alt="<?php the_title(); ?>" />
 	</div>

 	<div id="main">
 		<div id="welcomeSection">
 			<?php
	  			$args = array(
	    		'post_type' => 'welcome-text',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
 			<h1><?php the_field('title'); ?></h1>
 			<p><?php the_field('welcome_text'); ?></p>

	 		<?php
				}
					}
				else {
				echo 'No Text Found';
				}
			?>
 		</div>
 		<div id="doctor"></div>

 			<?php
	  			$args = array(
	    		'post_type' => 'meetourdoctors',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

		<div class="meetSection" id="meetTheDoctor">
	 		<div id="meetImage">
	 			<img src="<?php the_field('image'); ?>" />
	 		</div>
	 		<div id="meetText">
	 			<h1><?php the_field('doctor'); ?></h1>
				<p><?php the_field('text'); ?></p>
	 		</div>
		</div>

		<?php
			}
				}
			else {
			echo 'No Doctors Found';
			}
		?>
		<div id="policy"></div>
		<div id="policySection">
			<h1>Office Policies</h1>
 			<?php
	  			$args = array(
	    		'post_type' => 'office-policy',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<?php the_field('office_policy'); ?>

		<?php
			}
				}
			else {
			echo 'No Text Found';
			}
		?>
 		</div>
 		<div id="hours"></div>
 		 	<?php
	  			$args = array(
	    		'post_type' => 'office-hours',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
 		<div id="hoursSection">
 			<h2><?php the_field('office_name'); ?></h2>
 			<div id="hoursIcon">
 				<img src="<?php bloginfo('template_directory'); ?>/images/clock-icon.png">
 				<h1>Hours</h1>	
 			</div>

 			<div id="hoursSchedule">



 				<table>
 
				  <tr>
				    <td>Monday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('monday'); ?></td>
				  </tr>

				  <tr>
				    <td>Tuesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('tuesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Wednesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('wednesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Thursday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('thursday'); ?></td>
				  </tr>

				  <tr>
				    <td>Friday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('friday'); ?></td>
				  </tr>

				  <tr>
				    <td>Saturday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('saturday'); ?></td>
				  </tr>

				  <tr>
				    <td>Sunday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('sunday'); ?></td>
				  </tr>

				</table>

 			</div> <!-- hoursSchedule -->
 			</div> <!-- hoursSection -->
			<?php
				}
					}
				else {
				echo 'No Text Found';
				}
			?>


 			<div id="plansSection">
 				<h1>Insurance Plans</h1>
 				<div id="plansProviders">
 			<?php
	  			$args = array(
	    		'post_type' => 'insurance-plans',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
				<?php the_field('insurance_content'); ?>
 			<?php
				}
					}
				else {
				echo 'No Plans Found';
				}
			?>	
 				</div>	
 			</div>

 			
 			<div id="plansSection">
 				<h1>Insurance Plans</h1>			
 				<div id="plansProviders">
 					<ul>
 					<?php
						global $post;
						$tmp_post = $post;
						$myposts = get_posts('numberposts=100&offset=0&category=5');  
						foreach($myposts as $post) :
						setup_postdata($post);
					?>
 						<li>
 							<b><?=$post->post_title?></b> <br /><?=$post->post_content?>
 						</li>
 						<?php endforeach; ?>
				  <?php $post = $tmp_post; ?>
 					</ul>	
 				</div>	
 			</div>
 			<div id="contact"></div>
 			<div id="contactSection">

 				<h1>Contact Us</h1>
 				<h6 class="email-link">
 					</h6>
 				<div class="contactDivs">
	 				<div class="contactBlock">
	 					<?$id = 42;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/pinpoint-icon.png">
	 					
	 					<?=$content?>

	 				</div>
	 				<div class="contactBlock contactPhone">
	 					<?$id = 45;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/phone-icon.png">
	 					<h6><?=$content?></h6>

	 				</div>
	 				<div class="contactBlock">
	 					<img src="<?php bloginfo('template_directory'); ?>/images/map-icon.png">
	 					<!-- <span class=""><?php echo do_shortcode("[gmw id=2]"); ?></span> -->
	 					<a class="gmw-thumbnail-map gmw-lightbox-enabled" href="#gmw-dialog-googlemapswidget-2" title="Click to open larger map"><h3>View Map</h3></a>
	 						
	 				</div>
				</div>

 				<div class="contactChecker">
 					<div class="contactCheckerButton">
 						<h2><?php echo do_shortcode("[links category_name=SymptomChecker]"); ?></h2>
 					</div>
 					<div class="contactCheckerButton">
 						<h2><?php echo do_shortcode("[links category_name=DosageChecker]"); ?></h2>
 					</div>	
 				</div>


 				<div id="sponsorBox">
 					<img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png">
 					<h6>Allied Physicians Group</h6>
 					<button><?php echo do_shortcode("[links category_name=Visit]"); ?></button>
 				</div>

 			</div>

<?php get_footer(); ?>
